All [typewriterjs methods](https://github.com/tameemsafi/typewriterjs#methods) are available using a ref and the typewriter data, here is an example of a programmatic typing with `typeString`, `deleteAll` and `callFunction` :

```js
new Vue({
  template: `
  <div>
    <button
      @click="write">
      Type 'Write my type'
    </button>
    <button
      @click="erase">
      Erase and callback
    </button>

    <TypeWriter
      ref="tw" />

  </div>`,
  methods: {
    write () {
      this.$refs.tw.typewriter.typeString('Write my type').start()
    },
    erase () {
      this.$refs.tw.typewriter.deleteAll().callFunction(() => this.callback()).start()
    },
    callback () {
      alert('All clear')
    }
  }
})
```
