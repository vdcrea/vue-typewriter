Vue component to animate text with [typewriterjs](https://github.com/tameemsafi/typewriterjs).

[Demos and documentation](https://vdcrea.gitlab.io/vue-typewriter/)  
[Gitlab repository](https://gitlab.com/vdcrea/vue-typewriter/)

Install
```html
npm i -S gitlab:vdcrea/vue-typewriter
```

Use globally:
```html
import Vue from 'vue'
import VueTypewriter from 'vue-typewriter'
Vue.use(VueTypewriter)
```
