import TypeWriter from './components/TypeWriter'

// Install the components
export function install (Vue) {
  Vue.component('TypeWriter', TypeWriter)
}

// Expose the components
export {
  TypeWriter
}

/* -- Plugin definition & Auto-install -- */
/* You shouldn't have to modify the code below */

// Plugin
const plugin = {
  version: process.env.VUE_APP_VERSION,
  install
}

export default plugin

// Auto-install
let GlobalVue = null
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue
}
if (GlobalVue) {
  GlobalVue.use(plugin)
}
