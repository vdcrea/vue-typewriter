Simple example:

```
<TypeWriter>Your text here</TypeWriter>
```

Example with a custom cursor and speed:

```
<TypeWriter
  loop
  :delay="1000"
  cursor="◼">
  Custom cursor and speed
</TypeWriter>
```

You can change any of the props, the typewriter script will restart.

```js
const randomWords  = require('random-words')

new Vue({
  template: `
  <div>
    <button
      @click="resetText">
      Write other strings
    </button>

    <pre>{{ texts }}</pre>

    <TypeWriter
      autoStart
      :strings="texts" />

  </div>`,
  data () {
    return {
      texts: randomWords({ min: 3, max: 10 })
    }
  },
  methods: {
    resetText () {
      this.texts = randomWords({ min: 3, max: 10 })
    }
  }
})
```

You can update the content of the component, the text will be deleted and rewritten with the new content:

```js
new Vue({
  template: `
  <div>
    <button
      @click="write">
      Write another thing
    </button>

    <TypeWriter>
      {{ text }}
    </TypeWriter>

  </div>`,
  data () {
    return {
      text: 'Something'
    }
  },
  methods: {
    write () {
      if (this.text === 'Something') this.text = 'Another thing'
      else this.text = 'Something'
    }
  }
})
```
