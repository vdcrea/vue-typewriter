const path = require('path')

module.exports = {
  title: 'Vue Typewriter',
  components: 'src/components/**/[A-Z]*.vue',
  usageMode: 'expand',
  editorConfig: {
    theme: 'material',
  },
  styleguideDir: 'public',
	getComponentPathLine(componentPath) {
    const component = path.basename(componentPath, '.vue').split('.')[0]
    const dir = path.dirname(componentPath)
    return `import { ${component} } from 'vue-typewriter'`
  },
  sections: [
    {
      name: 'Vue Typewriter',
      content: 'readme.md'
    },
    {
      name: 'Demos',
      components: 'src/components/TypeWriter.vue'
    },
    {
      name: 'Methods',
      content: 'readme-methods.md'
    }
  ]
}
